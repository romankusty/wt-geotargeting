"use strict";

function WtLocationClass(){
    var self = this;

    var data = {}; // Текущий регион

    /* Получить текущий регион */
    this.getValue = function(type){
        if (self.data === undefined) return false;
        if (self.data[type] === undefined) return false;
        return self.data[type];
    }

    /* Присвоить новый регион в виде массива значений.  */
    this.setValues = function(values, redirect){
        document.body.style.cursor = 'wait';

        self.data = values;

        dataSaveInCookie();

        if (redirect == 'reload') location.reload(true);
        else if (redirect !== undefined) location.href = redirect;
        else document.body.style.cursor = 'auto';
    }

    /* Присвоить новый регион. Присваивая один тип, остальные типы обнуляются. */
    this.setValue = function(name, type, redirect){
        document.body.style.cursor = 'wait';

        self.data = {};

        self.data[type] = name;
        dataSaveInCookie();

        if (redirect == 'reload') window.location.reload(true);
        else if (redirect !== undefined) location.href = redirect;
        else document.body.style.cursor = 'auto';
    }

    this.setCountry = function(name, redirect){
        self.setValue(name, 'country', redirect);
    }

    this.setDistrict = function(name, redirect){
        self.setValue(name, 'district', redirect);
    }

    this.setRegion = function(name, redirect){
        self.setValue(name, 'region', redirect);
    }

    this.setCity = function(name, redirect){
        self.setValue(name, 'city', redirect);
    }

    this.setAdministrativeDistrict = function(name, redirect){
        self.setValue(name, 'administrative_district', redirect);
    }

    /* Сохранить значения региона из cookie */
    function dataReloadOfCookie(){
        var data_json = getCookie('wt_geo_data');
        if (data_json === undefined) return false;

        // Устраняем конфликт с плагином "WooCommerce Customer Relationship Manager"
        data_json = decodeURIComponent(data_json);

        self.data = JSON.parse(data_json);
    }

    /* Сохранить текущие значения региона в cookie */
    function dataSaveInCookie(){
        var data_json = JSON.stringify(self.data);
        setCookie('wt_geo_data', data_json, {expires: 3600 * 24 * 7});
    }

    dataReloadOfCookie();
}

var WtLocation;

jQuery(document).ready(function()
{
    WtLocation = new WtLocationClass();
});