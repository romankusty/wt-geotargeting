<?php

/**
 * Локации
 * User: Roman Kusty
 * Date: 26.02.2017
 */
class WtLocation
{
    public $id;

    public $parameter = array();

    static $table_relation_post = 'location_relationships';

    function __construct($id = null)
    {
        if (!empty($id)){
            $this->id = $id;
            $this->setParametersFromMeta($id);
        }
        // Открываем доступ к коду через статический класс WT плагина WT KIT
        if (class_exists('Wt')){
            Wt::setObject('location', $this);
        }


        add_action('wp_ajax_search_location', array($this, 'ajax_search_locations'));           // Привязка ajax-события search_location
        add_action('wp_ajax_nopriv_search_location', array($this, 'ajax_search_locations'));    // Привязка ajax-события search_location
    }

    public function initialAdmin(){

    }

    public function setParametersFromMeta($id){
        $this->setValue('region_id', $id);

        $meta = get_metadata('post', $id);

        foreach ($meta as $key => $value){
            $this->setValue($key, $value[0]);
        }
    }



    public function setValue($name, $value){
        $this->parameter[$name] = $value;
    }

    public function getValue($attribute){
        if (empty($this->parameter[$attribute])) return null;

        return $this->parameter[$attribute];
    }

    static function getObjects($options = array()){
        $args = array(
            'post_type' => 'region',
            'post_status' => 'publish',
            'meta_query' => array(),
            'posts_per_page' => -1
        );

        if (!empty($options['id'])) $args['page_id'] = $options['id'];
        if (!empty($options['fields'])) $args['fields'] = $options['fields'];

        if (!empty($options['name'])) $args['title'] = $options['name'];
        if (!empty($options['region_name'])) $args['title'] = $options['region_name'];

        if (!empty($options['region_type'])) $args['meta_query'][] = array(
            'key'     => 'region_type',
            'value'   => $options['region_type']
        );

        if (!empty($options['subdomain'])) $args['meta_query'][] = array(
            'key'     => 'subdomain',
            'value'   => $options['subdomain']
        );


        if (!empty($options['slug'])) $args['name'] = $options['slug'];
        if (!empty($options['post_parent'])) $args['post_parent'] = $options['post_parent'];

        if (!empty($options['country_iso'])) $args['meta_query'][] = array(
            'key'     => 'country_iso',
            'value'   => $options['country_iso']
        );


        if (!empty($options['args']) && is_array($options['args'])) $args = array_merge ($args, $options['args']);

        $query = new WP_Query($args);

        return $query->posts;
    }


    static function getObject($options = array())
    {
        $posts = self::getObjects($options);

        if (empty($posts[0])) return null;

        return $posts[0];
    }

    /**
     * Получить город
     *
     * @param $name
     * @param array $params
     * @return null
     */
    public function getCity($name, $options = array()){
        $options['type'] = 'city';
        $options['name'] = $name;

        return $this->getObject($options);
    }

    public function getCityId($name, $options = array())
    {
        $name = trim($name);

        $region = $this->getCity($name, $options);

        if (empty($region->ID)) return null;

        return $region->ID;
    }

    static function getNameById($id)
    {
        $location_object = self::getObject(array('id' => $id));

        if (empty($location_object->post_title)) return false;

        return $location_object->post_title;
    }






    /**
     * Добавить связь со страницей
     *
     * @param $post_id
     */
    function insertPost($post_id){
        $this->insertItemLocationPost($this->id, $post_id);
    }

    /**
     * Удалить связь со страницей
     *
     * @param $post_id
     */
    function deletePost($post_id){
        $this->deleteItemLocationPost($this->id, $post_id);
    }

    /**
     * Получить массив ID страниц, прикрепленных к текущему региону
     *
     * @return mixed
     */
    function getPostsId(){
        global $wpdb;

        $table_name = $wpdb->get_blog_prefix() . self::$table_relation_post;

        $posts = $wpdb->get_col(
            $wpdb->prepare('SELECT object_id FROM ' . $table_name . ' WHERE `location_id` = %d AND `object_type` = "post";', $this->id)
        );

        return $posts;
    }

    static function getLocationsIncludingNeighborsId($id){
        // Получаем родителя локации
        $location = self::getObject(array('id' => $id));

        if (empty($location->post_parent)) return array($id);

        // Получаем все вложенные локации родителя
        $locations = self::getObjects(array(
            'post_parent' => $location->post_parent,
            'fields' => 'ids'
        ));
        return $locations;

    }

    /**
     * Создание таблицы "Связь локации и поста"
     * Поле object_type может принимать значения: post, user
     *
     */
    static function createTableLocationRelationships(){
        global $wpdb;

        $table_name = $wpdb->get_blog_prefix() . self::$table_relation_post;

        $charset_collate = 'DEFAULT CHARACTER SET ' . $wpdb->charset .' COLLATE ' . $wpdb->collate;

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

        $sql = 'CREATE TABLE ' . $table_name . ' (
            location_id bigint(20) NOT NULL,
            object_id bigint(20) NOT NULL,
            object_type varchar(10) NOT NULL,
            PRIMARY KEY  (location_id, object_id, object_type)
        ) ' . $charset_collate .';';

        dbDelta($sql);
    }

    /**
     * Добавить связь между страницей и локацией
     *
     * @param $location_id
     * @param $post_id
     */
    function insertItemLocationPost($location_id, $post_id){
        global $wpdb;

        $table_name = $wpdb->get_blog_prefix() . self::$table_relation_post;

        $wpdb->insert(
            $table_name,
            array(
                'location_id' => $location_id,
                'object_id' => $post_id,
                'object_type' => 'post'
            ),
            array('%d', '%d')
        );
    }

    static function insertLocationRelationships($location_id, $object_id, $object_type = 'post'){
        global $wpdb;

        $table_name = $wpdb->get_blog_prefix() . self::$table_relation_post;

        $result = $wpdb->insert(
            $table_name,
            array(
                'location_id' => $location_id,
                'object_id' => $object_id,
                'object_type' => $object_type
            ),
            array('%d', '%d', '%s')
        );

        return $result;
    }

    /**
     * Удалить связь между страницей и локацией
     *
     * @param $location_id
     * @param $post_id
     */
    function deleteItemLocationPost($location_id, $post_id){
        global $wpdb;

        $table_name = $wpdb->get_blog_prefix() . self::$table_relation_post;

        $wpdb->delete(
            $table_name,
            array(
                'location_id' => $location_id,
                'object_id' => $post_id,
                'object_type' => 'post'
            ),
            array('%d', '%d')
        );
    }

    function ajax_search_locations(){
        // Очищаем входящие данные
        $value = sanitize_text_field($_POST['value']);

        if (isset($_POST['data_type'])) $data_type = sanitize_text_field($_POST['data_type']);
        else $data_type = 'array';

        // Поиск подходящих городов
        if ($data_type == 'array'){
            $cities = Wt::$obj->contacts->getRegionsArray(
                array(
                    'filter' => array(
                        'type' => 'city'
                    ),
                    'title' => $value
                )
            );
        }elseif ($data_type == 'object'){
            $cities = Wt::$obj->contacts->getRegionsArray(
                array(
                    'item_type' => 'object',
                    'title' => $value,
                    'filter' => array(
                        'type' => 'city')
                )
            );
        }


        echo json_encode($cities);

        die;
    }
}