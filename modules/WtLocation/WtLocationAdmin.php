<?php

/**
 * Class WtLocationAdmin
 */
class WtLocationAdmin extends WtGtAdminBehavior
{
	private $cache_city = array();
	private $cache_region = array();
	private $cache_district = array();


	function __construct(){
		add_action('admin_menu', array(&$this, 'menu'));
		add_action('admin_init', array(&$this, 'settingsRegister'));
	}

	public function menu(){
		add_submenu_page(
			'wt_geotargeting',
			'WT GeoTargeting - Импорт',
			'Импорт',
			'manage_options',
			'import',
			array(&$this, 'optionsPageOutput')
		);
	}

	// ---------- НАСТРОЙКА ----------

	/**
	 * Регистрируем настройки.
	 * Настройки будут храниться в массиве, а не одна настройка = одна опция.
	 */
	function settingsRegister(){
		// $option_group, $option_name, $sanitize_callback
		register_setting('wt_geotargeting_import_group', 'wt_geotargeting_import', array(&$this, 'sanitizeCallback'));
		register_setting('wt_geotargeting_import_group', 'wt_geotargeting_import_not_exist', array(&$this, 'sanitizeCallback'));

		add_settings_section(
			'wt_geotargeting_import',
			'',
			'',
			'wt_geotargeting_import_page');

		$field_params = array(
			'type'      => 'select', // тип
			'id'        => 'import_item_duplicate',
			'option_name' => 'wt_geotargeting_import',
			'label_for' => 'import_item_duplicate',
			'vals'		=> array(
				0 => 'Пропускать',
//				1 => 'Создавать дубликат',
//				2 => 'Перезаписывать'
			),
		);
		add_settings_field('import_item_duplicate', 'При совпадении городов', array(&$this, 'displaySettings'), 'wt_geotargeting_import_page', 'wt_geotargeting_import', $field_params);

	}

	/**
	 * Создаем страницу настроек публикаций
	 */
	public function optionsPageOutput(){
		?>
		<div class="wrap">
			<h2><?php echo get_admin_page_title() ?></h2>

			<form action="options.php" enctype="multipart/form-data" method="POST">
				<table class="form-table">
					<tbody>
					<tr>
						<th scope="row"><label for="import_file_upload">Файл импорта</label></th>
						<td><input name="wt_geotargeting_import_file_upload" type="file"><br><span
								class="description">Загрузите файл с перечнем городов в формате IpGeoBase (cities.txt)</span>
						</td>
					</tr>
					</tbody>
				</table>
				<?php
				settings_fields('wt_geotargeting_import_group');     // скрытые защитные поля
				do_settings_sections('wt_geotargeting_import_page'); // секции с настройками (опциями).
				do_settings_sections('wt_geotargeting_import_not_exist_page'); // секции с настройками (опциями).
				submit_button();
				?>
			</form>
		</div>
		<?php
	}

	function sanitizeCallback($value) {
		if (!empty($_FILES['wt_geotargeting_import_file_upload'])){
			$file_upload = $_FILES['wt_geotargeting_import_file_upload'];

			if (($handle = fopen($file_upload["tmp_name"], "r")) !== FALSE) {

				$row = 1;
				while (($data = fgetcsv($handle, 1000, "\t")) !== FALSE) {
					// 0 - ID IpGeoBase
					// 1 - Город
					// 2 - Регион
					// 3 - Округ
					// 4 - Координата
					// 5 - Координата

					$ipgeobase_id = $data[0];
					$city_name = iconv('CP1251','UTF-8',$data[1]);
					$region_name = iconv('CP1251','UTF-8',$data[2]);
					$district_name = iconv('CP1251','UTF-8',$data[3]);

					// Проверяем наличие округа в кэше для сокращения количества обращений к БД
					$post_district_id = array_search($district_name, $this->cache_district);

					if (empty($post_district_id)){
						$post_district = WT::$obj->contacts->getRegion($district_name, array('type' => 'district'));

						if (empty($post_district)){
							$post_district_values = array(
								'post_type' => 'region',
								'post_title' => $district_name,
								'post_status' => 'publish',
								'meta_input' => array(
									'region_type' => 'district'),
								'post_author'   => 1
							);

							$post_district_id = wp_insert_post($post_district_values);
						}else{
							$post_district_id = $post_district->ID;
						}

						// Сохраняем значение округа в кэш
						$this->cache_district[$post_district_id] = $district_name;
					}

					// Проверяем наличие региона в кэше для сокращения количества обращений к БД
					$post_region_id = array_search($region_name, $this->cache_region);

					if (empty($post_region_id)){
						$post_region = WT::$obj->contacts->getRegion($region_name, array('type' => 'region'));

						if (empty($post_region)){
							$post_region_values = array(
								'post_type' => 'region',
								'post_title' => $region_name,
								'post_status' => 'publish',
								'post_parent' => $post_district_id,
								'meta_input' => array(
									'region_type' => 'region'),
								'post_author'   => 1
							);

							$post_region_id = wp_insert_post($post_region_values);
						}else{
							$post_region_id = $post_region->ID;
						}

						// Сохраняем значение региона в кэш
						$this->cache_region[$post_region_id] = $region_name;
					}

					// Проверяем наличие города в кэше для сокращения количества обращений к БД
					$post_city_id = array_search($city_name, $this->cache_city);

					if (empty($post_city_id)){
						$post_city = WT::$obj->contacts->getRegion($city_name, array('type' => 'city'));

						if (empty($post_city)){
							$post_city_values = array(
								'post_type' => 'region',
								'post_title' => $city_name,
								'post_status' => 'publish',
								'post_parent' => $post_region_id,
								'meta_input' => array(
									'region_type' => 'city',
									'ipgeobase_id' => $ipgeobase_id),
								'post_author'   => 1
							);

							$post_city_id = wp_insert_post($post_city_values);
						}else{
							$post_city_id = $post_city->ID;
						}

						// Сохраняем значение города в кэш
						$this->cache_city[$post_city_id] = $city_name;
					}
				}
				fclose($handle);
			}
		}

		return $value;
	}
}
?>