<?php
/*
Plugin Name: WT Geotargeting Pro
Plugin URI: http://web-technology.biz/cms-wordpress/plugin-wt-geotargeting
Description: Набор инструментов для настройки геотаргетинга.
Version: 1.6.9
Author: Кусты Роман, АИТ "WebTechnology"
Author URI: http://web-technology.biz
*/

define('WT_GT_PRO_PLUGIN_FILE', __FILE__);
define('WT_GT_PRO_PLUGIN_DIR', dirname(WT_GT_PRO_PLUGIN_FILE));
define('WT_GT_PRO_PLUGIN_BASENAME', plugin_basename(WT_GT_PRO_PLUGIN_FILE));

require_once(WT_GT_PRO_PLUGIN_DIR . '/vendor/autoload.php');
include(WT_GT_PRO_PLUGIN_DIR . '/includes/ipgeobase.php');	// Класс для работы с IpGeoBase

include(WT_GT_PRO_PLUGIN_DIR . '/includes/wt_kit.php');      // Статический класс и набор инструментов
include(WT_GT_PRO_PLUGIN_DIR . '/includes/wt_data_files.php');

require(WT_GT_PRO_PLUGIN_DIR . '/includes/wt_gt_admin_behavior.php');

include(WT_GT_PRO_PLUGIN_DIR . '/includes/wt_initialization.php'); // Настройка библиотек
include(WT_GT_PRO_PLUGIN_DIR . '/includes/WtGeolocation.php');		// Оболочка для работы с Web-сервисами
include(WT_GT_PRO_PLUGIN_DIR . '/modules/wt_contacts/wt_contacts.php'); // Работа с контактами
include(WT_GT_PRO_PLUGIN_DIR . '/modules/WtLocation/WtLocation.php'); // Работа с локациями
include(WT_GT_PRO_PLUGIN_DIR . '/modules/wt_mail/wt_mail.php'); 	// Работа с email-рассылкой

class WtGeoTargetingPro
{
    public $modules = array(
//        'wt_contacts' => [
//            'name' => 'Регионы и Контакты',
//            'description' => 'Ручное добавление регионов и закрепление за ними контактной информации.'
//        ],
//        'wt_gt_nav_menu' => [
//            'name' => 'Меню',
//            'description' => 'Настройка геотаргетинга в меню.',
//            'class' => 'WtGtNavMenu'
//        ],
//        'WtLocation' => [
//            'name' => 'Локации',
//            'description' => ''
//        ],
//        'wt_mail' => [
//            'name' => 'Email',
//            'description' => 'Отправка email-писем на региональные почтовые ящики.'
//        ],
        'WtGtRegion' => array(
            'name' => 'Регионы'
        ),
        'WtGtPost' => array(
            'name' => 'Публикации и страницы',
            'description' => 'Создание связи между публикациями и локациями (регионами).',
            'class' => 'WtGtPost'
        ),
        'WtGtSubdomain' => array(
            'name' => 'Поддомены',
            'description' => 'Обработка обращений со входящих поддоменов.',
            'class' => 'WtGtSubdomain'
        ),
        'WtGtUser' => array(
            'name' => 'Пользователи',
            'description' => 'Взаимодействие с пользователями.',
            'class' => 'WtGtUser'
        ),
    );

    function __construct(){
        add_action('plugins_loaded', array($this, 'pluginsLoaded'));
        add_action('init', array($this, 'initial'));
    }

    static function activation(){
        WtLocation::createTableLocationRelationships();
    }

    static function deactivation(){}

    public function uninstall(){}

    function pluginsLoaded(){
        if (defined('ABSPATH') && is_admin()) $this->initialAdmin();

        $this->activationModules();

        new WtContacts();
        new WtMail();

        require(WT_GT_PRO_PLUGIN_DIR . '/modules/wt_gt_nav_menu/wt_gt_nav_menu.php');
        new WtGtNavMenu();


        new WtInitialization();
    }

    function initial(){
        // Регистрация скриптов для фронтенда
        add_action('wp_enqueue_scripts', array($this, 'registerScripts'));

        new WtLocation();
    }

    public function initialAdmin(){
        require(WT_GT_PRO_PLUGIN_DIR . '/includes/wt_gt_admin.php');
        require(WT_GT_PRO_PLUGIN_DIR . '/modules/wt_contacts/wt_contacts_admin.php');

        // Регистрация скриптов для админки
        add_action('admin_enqueue_scripts', array($this, 'registerAdminScripts'));

        $wt_gt_pro_admin = new WtGtAdmin();
        $wt_gt_pro_admin->geotargeting = $this;

        new WtContactsAdmin();

        // Настройка Меню
        require(WT_GT_PRO_PLUGIN_DIR . '/modules/wt_gt_nav_menu/wt_gt_nav_menu_admin.php');
        new WtGtNavMenuAdmin();

        require(WT_GT_PRO_PLUGIN_DIR . '/modules/WtLocation/WtLocationAdmin.php');
        new WtLocationAdmin();
    }

    public function activationModules(){
        foreach ($this->modules as $key => $module){
            require(WT_GT_PRO_PLUGIN_DIR . '/modules/' . $key . '/' . $key . '.php');
            new $key();
        }
    }

    public function registerScripts(){
        wp_register_script(
            'wt-cookie',
            plugin_dir_url(WT_GT_PRO_PLUGIN_FILE) . '/js/cookie.js',
            array('jquery'),
            '0.2.0');

        wp_register_script(
            'wt-location',
            plugin_dir_url(WT_GT_PRO_PLUGIN_FILE) . '/js/wt-location.js',
            array('jquery', 'wt-cookie'),
            '0.1.1');
        wp_enqueue_script('wt-location');
    }

    public function registerAdminScripts(){
        wp_register_script(
            'checkboxes',
            plugin_dir_url(WT_GT_PRO_PLUGIN_FILE) . '/libs/jquery.checkboxes-1.2.0.min.js',
            array('jquery'),
            '1.2.0'
        );
    }
}

new WtGeoTargetingPro();
register_activation_hook(WT_GT_PRO_PLUGIN_FILE, array('WtGeoTargetingPro', 'activation'));
register_deactivation_hook(WT_GT_PRO_PLUGIN_FILE, array('WtGeoTargetingPro', 'deactivation'));
register_uninstall_hook(WT_GT_PRO_PLUGIN_FILE, array('WtGeoTargetingPro', 'uninstall'));